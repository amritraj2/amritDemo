package com.demo.extension.fynd.config;

import com.demo.extension.fynd.common.CommonConstants;
import com.fynd.extension.service.ExtensionService;
import com.sdk.application.ApplicationClient;
import com.sdk.platform.PlatformClient;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;

@Configuration
public class FyndClientConfig {
  @Autowired ExtensionService extensionService;

  @Value("${ext.application_id}")
  private String applicationId;

  @Value("${ext.company_id}")
  private String companyId;

  @Value("${ext.application_token}")
  private String applicationToken;

  public FyndClient getFyndClient() {
    try {
      PlatformClient platformClient = extensionService.getPlatformClient(companyId);
      PlatformClient.ApplicationClient applicationClient =
          platformClient.application(applicationId);
      return new FyndClient(platformClient, applicationClient);
    } catch (Exception err) {
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "PlatformClient Error");
    }
  }

  public ApplicationClient getApplicationClient() {
    try {
      return extensionService.getApplicationClient(applicationId, applicationToken);
    } catch (Exception e) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR, "Application Client Error");
    }
  }

  public ApplicationClient getApplicationClient(HttpServletRequest request) {
    try {
      ApplicationClient applicationClient =
          extensionService.getApplicationClient(applicationId, applicationToken);
      HashMap<String, String> extraHeaders =
          applicationClient.getCart().getApplicationConfig().getExtraHeaders();
      extraHeaders.put(
          CommonConstants.X_USER_DATA,
          String.valueOf(request.getHeader(CommonConstants.X_USER_DATA)));
      extraHeaders.put(
          CommonConstants.COOKIE, String.valueOf(request.getHeader(CommonConstants.COOKIE)));
      return applicationClient;
    } catch (Exception e) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR, "Application Client Error");
    }
  }
}
