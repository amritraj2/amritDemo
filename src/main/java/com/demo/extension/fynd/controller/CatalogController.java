package com.demo.extension.fynd.controller;

import com.demo.extension.fynd.config.FyndClientConfig;
import com.sdk.application.ApplicationModels;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import retrofit2.http.Query;
import retrofit2.http.Url;

import java.io.IOException;


@Log4j2
@RestController
@RequestMapping("/products")
public class CatalogController {

    @Autowired
    private FyndClientConfig fyndClientConfig;

    @GetMapping("/list")
    ApplicationModels.ProductListingResponse getProducts( @Query("q") String q, @Query("f") String f, @Query("filters") Boolean filters,
                                                          @Query("sort_on") String sortOn, @Query("page_id") String pageId, @Query("page_size") Integer pageSize,
                                                          @Query("page_no") Integer pageNo, @Query("page_type") String pageType) throws IOException {



//    @GetMapping("/list")
//    public ApplicationModels.ProductListingResponse getProducts(
//            @RequestParam(name = "page_id", required = false, defaultValue = "*") String pageId,
//            @RequestParam(name = "page_no", required = false, defaultValue = "1") Integer pageNo,
//            @RequestParam(name = "page_size", required = false, defaultValue = "12") Integer pageSize,
//            @RequestParam(name = "page_type", required = false, defaultValue = "number") String pageType,
//            @RequestParam(name = "f", required = false, defaultValue = "") String filterString,
//            @RequestParam(name = "q", required = false, defaultValue = "") String query) throws IOException {
        ApplicationModels.ProductListingResponse response = fyndClientConfig
                .getApplicationClient()
                .catalog
                .getProducts(
                        q,
                        StringUtils.EMPTY,
                        true,
                        StringUtils.EMPTY,
                        StringUtils.EMPTY,
                        pageSize,
                        pageNo,
                        pageType);

        int i = 0;
        for (ApplicationModels.ProductListingDetail productListingDetail : response.getItems()) {
            log.info(productListingDetail.getPrice().getEffective());
            log.info(productListingDetail.getPrice().getMarked());

            if (i > 1) {
                break;
            }
            i++;
        }

        return response;
    }
}
