package com.demo.extension.fynd.service.impl;

import com.demo.extension.fynd.common.DataLoaderProperties;
import com.demo.extension.fynd.config.FyndClientConfig;
import com.demo.extension.fynd.service.DataLoaderService;
import com.sdk.common.model.FDKException;
import com.sdk.platform.PlatformClient;
import com.sdk.platform.PlatformModels;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DataLoaderServiceImpl implements DataLoaderService {

  @Autowired private FyndClientConfig fyndClientConfig;

  @Autowired private DataLoaderProperties dataLoaderProperties;

  @Override
  public Object setDataLoader() {
    List<PlatformModels.DataLoaderResponseSchema> keywords = null;
    PlatformClient.ApplicationClient applicationClient =
        fyndClientConfig.getFyndClient().getApplicationClient();

    List<DataLoaderProperties> dataloaders = new ArrayList<>();

    //     Dataloader 1
    DataLoaderProperties dataloader1 = new DataLoaderProperties();
    dataloader1.setName("Ajio_Products");
    dataloader1.setService("catalog");
    dataloader1.setOperationId("getProducts");
    dataloader1.setType("url");
    dataloader1.setUrl("/ext/search-api/products/list");
    dataloaders.add(dataloader1);
/*

    // Dataloader 2
    DataLoaderProperties dataloader2 = new DataLoaderProperties();
    dataloader2.setName("DMS_getCategories");
    dataloader2.setService("catalog");
    dataloader2.setOperationId("getCategories");
    dataloader2.setType("url");
    dataloader2.setUrl("/ext/dms-api/catalog/categories");
    dataloaders.add(dataloader2);

    // Dataloader 3
    DataLoaderProperties dataloader3 = new DataLoaderProperties();
    dataloader3.setName("DMS_getBrands");
    dataloader3.setService("catalog");
    dataloader3.setOperationId("getBrands");
    dataloader3.setType("url");
    dataloader3.setUrl("/ext/dms-api/catalog/brands");
    dataloaders.add(dataloader3);

    // Dataloader 4
    DataLoaderProperties dataloader4 = new DataLoaderProperties();
    dataloader4.setName("DMS_getProductSizesBySlug");
    dataloader4.setService("catalog");
    dataloader4.setOperationId("getProductSizesBySlug");
    dataloader4.setType("url");
    dataloader4.setUrl("/ext/dms-api/catalog/products/{slug}/sizes");
    dataloaders.add(dataloader4);

    //     Dataloader 5
    DataLoaderProperties dataloader5 = new DataLoaderProperties();
    dataloader5.setName("DMS_getCart");
    dataloader5.setService("cart");
    dataloader5.setOperationId("getCart");
    dataloader5.setType("url");
    dataloader5.setUrl("/ext/dms-api/cart/detail");
    dataloaders.add(dataloader5);

    // Dataloader 6
    DataLoaderProperties dataloader6 = new DataLoaderProperties();
    dataloader6.setName("DMS_addItemsCart");
    dataloader6.setService("cart");
    dataloader6.setOperationId("addItems");
    dataloader6.setType("url");
    dataloader6.setUrl("/ext/dms-api/cart/detail");
    dataloaders.add(dataloader6);

    // Dataloader 7
    DataLoaderProperties dataloader7 = new DataLoaderProperties();
    dataloader7.setName("DMS_updateCart");
    dataloader7.setService("cart");
    dataloader7.setOperationId("updateCart");
    dataloader7.setType("url");
    dataloader7.setUrl("/ext/dms-api/cart/detail");
    dataloaders.add(dataloader7);

    // Dataloader 8
    DataLoaderProperties dataloader8 = new DataLoaderProperties();
    dataloader8.setName("DMS_getShipments");
    dataloader8.setService("cart");
    dataloader8.setOperationId("getShipments");
    dataloader8.setType("url");
    dataloader8.setUrl("/ext/dms-api/cart/shipment");
    dataloaders.add(dataloader8);
*/

    keywords =
        dataloaders
            .stream()
            .map(
                dataLoaderProperties1 -> {
                  try {
                    return applicationClient.content.addDataLoader(dataLoaderProperties1);
                  } catch (FDKException e) {
                    throw new RuntimeException(e);
                  }
                })
            .collect(Collectors.toList());

    return keywords;
  }

  @Override
  public Object getDataLoader() {
    PlatformModels.DataLoadersSchema keywords = null;
    try {
      PlatformClient.ApplicationClient applicationClient =
          fyndClientConfig.getFyndClient().getApplicationClient();
      keywords = applicationClient.content.getDataLoaders();
    } catch (FDKException e) {
      e.printStackTrace();
    }
    return keywords;
  }

  public Object deleteDataLoader(String dataLoaderId) {
    PlatformModels.DataLoaderResponseSchema keywords = null;
    try {
      PlatformClient.ApplicationClient applicationClient =
          fyndClientConfig.getFyndClient().getApplicationClient();
      keywords = applicationClient.content.deleteDataLoader(dataLoaderId);
    } catch (FDKException e) {
      e.printStackTrace();
    }
    return keywords;
  }
}
