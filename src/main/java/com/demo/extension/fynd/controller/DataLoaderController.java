package com.demo.extension.fynd.controller;

import com.demo.extension.fynd.service.DataLoaderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/data-loaders")
public class DataLoaderController {

  @Autowired private DataLoaderService dataLoaderService;

  @GetMapping("")
  public ResponseEntity getDataLoaders() {
    log.info("REST request to get data loaders ");
    return ResponseEntity.ok().body(dataLoaderService.getDataLoader());
  }

  @PostMapping("")
  public ResponseEntity setDataLoader() {
    log.info("REST request to set data loader ");
    return ResponseEntity.ok().body(dataLoaderService.setDataLoader());
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> setCategoryDataLoader(@PathVariable String id) {
    log.info("REST request to delete data loader ");
    return ResponseEntity.ok().body(dataLoaderService.deleteDataLoader(id));
  }
}
