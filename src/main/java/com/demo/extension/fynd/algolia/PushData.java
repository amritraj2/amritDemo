package com.demo.extension.fynd.algolia;

import com.algolia.search.DefaultSearchClient;
import com.algolia.search.SearchClient;
import com.algolia.search.SearchIndex;
import com.algolia.search.models.indexing.Query;
import com.algolia.search.models.indexing.SearchResult;

public class PushData {
    public static void main(String[] args) {
        SearchClient client = DefaultSearchClient.create("Z7A974LY46", "feb6201cab7b5ac741c4fcb35d291ed9");

                // Create a new index and add a record (using the `Record` class)
                SearchIndex<Record> index = client.initIndex("bond_007", Record.class);

//        Record record = new Record("Chinua Achebe local", "Nigeria local", "images/things-fall-apart.jpg",
//                "English", "https://en.wikipedia.org/wiki/Things_Fall_Apart",
//                209, "Things Fall Apart", 1958,"1");
//        index.saveObject(record).waitTask();

        // Search the index and print the results
        SearchResult<Record> results = index.search(new Query("Memoirs of Hadrian"));
        System.out.println(results.getHits().get(0));
    }
}
