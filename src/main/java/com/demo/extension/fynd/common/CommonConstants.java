package com.demo.extension.fynd.common;

public class CommonConstants {

  public static final String SELLER_NAME = "Seller Name";
  public static final String DISPOSITION = "Content-Disposition";
  public static final String OCTET_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
  public static final String BUYER_ID = "buyerPrmId";
  public static final String BUYER_TYPE = "buyerType";
  public static final String SCHEME_TYPE = "scheme_type";
  public static final String PRODUCT_TYPE = "product_type";
  public static final String LADDER_PRICE = "ladder_pricing";
  public static final String PRICE_TO_USER = "price_to_user";
  public static final String PRICE_TO_USER_DISCOUNT = "price_to_user_discount";
  public static final String MARGIN = "margin";
  public static final String SCHEME_DISCOUNT = "schemeDiscount";
  public static final String INJECTION_ID = "InjectionId";
  public static final String CESS = "cess";
  public static final String IGST = "igst";
  public static String EMAIL_REGEX = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
  public static String MOBILE_REGEX = "^$|[0-9]{10}";
  public static final String RIL_EMAIL =
      "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@(ril\\.com)$";
  public static final String SYSTEM_UPDATED = "SYSTEM_UPDATED";
  public static final String SUPER_ADMIN = "SUPER_ADMIN";
  public static final String X_USER_DATA = "x-user-data";
  public static final String USER_CHECKSUM_KEY = "user_checksum";
  public static final String PAYMENT_NAME = "COD";
  public static final String PAYMENT_MODE = "COD";
  public static final String PAYMENT_REQUIRED = "required";
  public static final String PAYMENT_GATEWAY_CHECKOUT = "Fynd";
  public static final String PAYMENT_META_TYPE = "aggregator";
  public static final String API_VERSION = "1.0";
  public static final String ERROR_MESSAGE = "errorMessage";
  public static final String ERROR_CODE = "errorCode";
  public static final String COOKIE = "cookie";
  public static final String CART_INJECTION_TYPE = "discount";
  public static final String CART_INJECTION_COLLECTION = "FYND";
  public static final String CART_INJECTION_MESSAGE = "DMS INJECTION";

  public static final String USER = "USER";
  public static final String BUILDING_NAME = "buildingName";
  public static final String STREET_NAME = "streetName";
  public static final String COMPLEX_NAME = "complex";
  public static final String PIN_CODE = "pincode";
  public static final String ADMIN = "ADMIN";
  public static final String CHANNEL_TYPE = "channelType";

  public static final String EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
  public static final String REQUEST_SUBMITTED = "Your request is submitted, with requestId: ";
  public static final String HIERARCHY_TEMPLATE = "RCPL Hierarchy Mapping Template";

  public static final String RCPL_SALES_TEMPLATE = "RCPL Sales Team";
  public static final String CFA_ID = "CFA ID";
  public static final String DB_ID = "DB ID";
  public static final String FOS_ID = "FOS ID";
  public static final String ID = "ID";
  public static final String ASE = "ASE";
  public static final String ASM = "ASM";
  public static final String STATE_HEAD = "State Head";
  public static final String REGIONAL_MANAGER = "Regional Manager";
  public static final String RHR = "RHR";
  public static final String REGIONAL_CAPACITY_DEVELOPMENT_MANAGER =
      "Regional Capacity Development Manager";
  public static final String NATIONAL_HEAD = "National Head";
  public static final String FOS_DB_MAPPING = "FOS - Distributor Mapping";
  public static final String RETAILER_FOS_MAPPING = "Retailer - FOS Mapping";

  public static final String CFA = "CFA";

  public static final String DB = "DB";

  public static final String FOS = "FOS";

  public static final String PAGE_TYPE_NUMBER = "number";
  public static final String PAGE_TYPE_CURSOR = "cursor";
  public static final Integer DEFAULT_PRODUCT_FETCH_SIZE = 12;

  public static final String COOKIE_STRING = "token=Bearer ";

  public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

  public static final String ALGORITHM_KEY = "AES";
  public static final String COOKIE_STRING_TTL_SECURE = "; Max-Age=36000; Path=/; Secure; HttpOnly";

  public static final String COOKIE_STRING_LOGOUT = "token=; Max-Age=0; Path=/; Secure; HttpOnly";

  public static final String BASE_64_PATTERN =
      "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$";

  public static final String CREDENTIAL_ID = "credentialId";

  public static final String SECRET_KEY = "secretKey";
  public static final String ERROR_IN_SERVICE = "Something went wrong";
  public static final String ERROR_OCCURRED = "Exception occurred in ";

  public static final String INCORRECT_MOBILE_NUMBER =
      "The  mobile number you entered is incorrect.";

  public static final String ADMIN_OTP =
      "Dear Applicant, use the OTP {{otp}} to register with JioMart Digital. This is a one-time code that is valid for 10 minutes.";

  public static final String TOO_MANY_TRIES = "OTP request rate limit reached";
  public static final String INCORRECT_OTP = "OTP entered is incorrect";

  public static final String CSV_FILE_NAME_FORMAT = "%s-%s.csv";

  public static final String EXCEL_FILE_NAME_FORMAT = "%s-%s.xlsx";

  public static final String AWS_NAMESPACE = "platform-extensions-private";

  public static final String EXTENSION_SLUG = "retailer-onboarding";
  public static final String ITEM_ID = "itemId";
  public static final String ITEM_SIZE = "itemSize";
  public static final String HSN = "hsn";
  public static final String CGST = "cgst";
  public static final String SGST = "sgst";
  public static final String LADDER_PRICE_TAG = "ladderPriceTag";
  public static final String ITEM_ID_PRICE_TAG = "itemIdPriceTag";
  public static final String PARENT_ORDER_ID = "parent_order_id";
  public static final String DISTRIBUTION_TYPE = "productDistributionType";
  public static final String SKU = "sku";

  public static final String APPROVES_ID = "approvesId";
  public static final String APPROVES_NAME = "approvesName";
  public static final String ROLE_TYPE = "roleType";
  public static final String LEVEL = "level";
  public static final String FOS_META_ID = "fos_id";
  public static final String APPROVES_LIST = "approves_list";
  public static final String SELLER_PRM_ID = "seller_prm_id";
  public static final String ORDER = "order";
  public static final String BEAT_ID = "beat_id";
  public static final String BUYER_PRM_ID = "buyer_prm_id";
  public static final String BUYER_STORE_ID = "buyer_store_id";
  public static final String SIMPLE_DATE_FORMATTER = "MM/dd/yyyy";
  public static final String BREAKUP_VALUES = "break_up_values";
  public static final String LOCATION = "location";
  public static final String EXCEL_FORMAT_DATE = "MM/dd/yyyy";
  public static final String DB_FORMAT_DATE = "yyyy-MM-dd";
}
