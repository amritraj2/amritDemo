package com.demo.extension.fynd.config;

import com.sdk.platform.PlatformClient;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FyndClient {
  private PlatformClient platformClient;
  private PlatformClient.ApplicationClient applicationClient;
}
