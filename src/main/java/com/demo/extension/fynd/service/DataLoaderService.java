package com.demo.extension.fynd.service;

public interface DataLoaderService {

  Object getDataLoader();

  Object setDataLoader();

  Object deleteDataLoader(String id);
}
