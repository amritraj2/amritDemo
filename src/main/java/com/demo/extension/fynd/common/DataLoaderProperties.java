package com.demo.extension.fynd.common;

import com.sdk.platform.PlatformModels;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "dataloader")
public class DataLoaderProperties extends PlatformModels.DataLoaderSchema {

  private String name;
  private String service;
  private String operationId;
  private String type;
  private String url;
}
