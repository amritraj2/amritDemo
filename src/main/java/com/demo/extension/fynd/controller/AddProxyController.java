package com.demo.extension.fynd.controller;


import com.fynd.extension.service.ExtensionService;
import com.sdk.platform.PlatformClient;
import com.sdk.platform.PlatformModels;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@Slf4j
@RestController
@RequestMapping("/api/add-proxy")
public class AddProxyController {

    @Autowired
    ExtensionService extensionService;

    @GetMapping
    public PlatformModels.AddProxyResponse addProxy() throws Exception {
        PlatformModels.AddProxyResponse res = null;
        PlatformModels.RemoveProxyResponse removeres = null;
        try {

            PlatformModels.AddProxyReq body = new PlatformModels.AddProxyReq();
            body.setAttachedPath("search-api");
            body.setProxyUrl("https://369c-116-50-84-114.ngrok-free.app");
            PlatformClient platformClient = extensionService.getPlatformClient("1");
            res = platformClient.application("64de0ffdcb64799d5d74584d").partner.addProxyPath("64c39d80024ab02616173833", body);

        } catch (HttpClientErrorException err) {
            err.printStackTrace();
        }

        return res;
    }

    @GetMapping("/remove")
    public  PlatformModels.RemoveProxyResponse removeProxy() throws Exception {
        PlatformModels.RemoveProxyResponse removeres = null;
        try {

//            PlatformModels.AddProxyReq body = new PlatformModels.AddProxyReq();
//            body.setAttachedPath("search-api");
//            body.setProxyUrl("https://amritdemo.jiox0.de");
            PlatformClient platformClient = extensionService.getPlatformClient("1");

            removeres = platformClient.application("64de0ffdcb64799d5d74584d").partner.removeProxyPath( "64c39d80024ab02616173833","search-api");

        } catch (HttpClientErrorException err) {
            err.printStackTrace();
        }

        return removeres;
    }
}
